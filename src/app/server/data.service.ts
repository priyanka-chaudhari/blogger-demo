import { Injectable } from '@angular/core';
import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class DataService implements InMemoryDbService {
  constructor() {}
  createDb() {
    let blogs = [
      { id: 1, blog_title: 'Angular', blog_description: 'It is Angular blog', blog_date: {year:2020, month:1, day:1} },
      { id: 2, blog_title: 'Python', blog_description: 'It is Python blog', blog_date: {year:2020, month:5, day:1} }
    ];

    return { blogs };
  }
}
