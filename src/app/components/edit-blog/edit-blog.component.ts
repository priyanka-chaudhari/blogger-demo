import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CommonSharedService } from 'src/app/service/common-shared.service';
import { BlogService } from 'src/app/service/blog.service';
import * as moment from 'moment';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css'],
})
export class EditBlogComponent implements OnInit {
  showData: boolean;
  model = { id: '', blog_title: '', blog_description: '', blog_date: '' };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _commonSharedService: CommonSharedService,
    private _blogService: BlogService
  ) {}

  ngOnInit(): void {
    let id = this.route.snapshot.paramMap.get('id');
    console.log(id);
    //common service
    if (parseInt(id) > 0) {
      this._blogService.getBlog(id).subscribe((data: any) => {
        this.showData = true;
        this.model = data;
      });

      this._commonSharedService.shareDataService$.subscribe((data) =>
        console.log(data)
      );
    }
  }
  update = (form) => {
    form.value.id = this.model.id;
    this._blogService.updateBlog(form.value).subscribe((data: any) => {
      console.log(data);
    });
    this.router.navigate(['/list-of-blogs']);
  };
}
