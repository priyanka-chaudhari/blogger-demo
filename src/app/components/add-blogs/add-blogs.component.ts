import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import {
  NgbCalendar,
  NgbDateAdapter,
  NgbDateParserFormatter,
  NgbDateStruct,
} from '@ng-bootstrap/ng-bootstrap';

import { CommonSharedService } from 'src/app/service/common-shared.service';
import { BlogService } from 'src/app/service/blog.service';

@Component({
  selector: 'app-add-blogs',
  templateUrl: './add-blogs.component.html',
  styleUrls: ['./add-blogs.component.css'],
})
export class AddBlogsComponent implements OnInit {
  model1: any;
  model = { blog_title: '', blog_description: '', blog_date: '' };

  constructor(
    private router: Router,
    private _commonSharedService: CommonSharedService,
    private _blogService: BlogService
  ) {}

  ngOnInit(): void {}
  gotoEdit = () => {
    this.router.navigate(['/edit-blog/', 2]); // navigate with parameter (para meter route)
    this._commonSharedService.addData({ id: 1, name: 'priyanka' }); // send common data
  };

  addBlog = (form) => {
    this._blogService.addBlog(form.value).subscribe((data) => {
      this.model = { blog_title: '', blog_description: '', blog_date: '' };
    });
    this.router.navigate(['/list-of-blogs']);
  };
}
