import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/service/blog.service';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list-of-blogs',
  templateUrl: './list-of-blogs.component.html',
  styleUrls: ['./list-of-blogs.component.css'],
})
export class ListOfBlogsComponent implements OnInit {
  blog_list = [];
  constructor(private _blogService: BlogService,
    private modalService: NgbModal) {}

  ngOnInit(): void {
    this.getAllBlogs();
  }
  getAllBlogs = () => {
    this._blogService.getAllBlogs().subscribe((blogs: []) => {
      this.blog_list = blogs;

      this.blog_list.map((element) => {
        const { year, month, day } = element.blog_date;

        element.blog_date = moment(new Date(year, month - 1, day)).format(
          'MM/DD/YYYY'
        );
      });
    });
  };

  deleteBlog(id,deleteReportPopUp){
    const mod = this.modalService.open(deleteReportPopUp);
    mod.result.then(data => {
      if (data === 'Yes') {
        this._blogService.deleteBlog(id).subscribe(() => {

        });
        this.getAllBlogs();
      }
    }, reason => {
      console.log(reason);
    });
  }
}
