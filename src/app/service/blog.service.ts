import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { catchError, retry, tap, map } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  API_URL: string = 'api/';
  headers = new HttpHeaders()
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers,
  };
  constructor(private httpClient: HttpClient) {}

  addBlog(value: any) {
    return this.httpClient
      .post(this.API_URL + 'blogs/', value, this.httpOptions)
      .pipe(
        tap((data) => console.log(data)),
        catchError(this.handleError)
      );
  }

  getAllBlogs() {
    return this.httpClient.get(this.API_URL + 'blogs/');
  }

  getBlog(id) {
    return this.httpClient.get(`${this.API_URL + 'blogs'}/${id}`);
  }

  updateBlog(value) {
    return this.httpClient
      .put(this.API_URL + 'blogs', value, this.httpOptions)
      .pipe(
        tap((data) => console.log(data)),
        catchError(this.handleError)
      );
  }

  deleteBlog(id) {
    return this.httpClient
      .delete(`${this.API_URL + 'blogs'}/${id}`);
  }

  private handleError(error: any) {
    console.error(error); //Created a function to handle and log errors, in case
    return throwError(error);
  }
}
