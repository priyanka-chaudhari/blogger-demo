import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CommonSharedService {
  private shareDataServie = new Subject<any>();
  shareDataService$ = this.shareDataServie.asObservable();
  constructor() {}

  addData = (data: any) => {
    this.shareDataServie.next(data);
  };
}
