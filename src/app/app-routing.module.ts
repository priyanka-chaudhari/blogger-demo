import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddBlogsComponent } from './components/add-blogs/add-blogs.component';
import { EditBlogComponent } from './components/edit-blog/edit-blog.component';
import { ListOfBlogsComponent } from './components/list-of-blogs/list-of-blogs.component';
import { AppComponent } from './app.component';

const routes: Routes = [
  { path: 'add-blog', component: AddBlogsComponent },
  { path: 'edit-blog/:id', component: EditBlogComponent },
  { path: 'list-of-blogs', component: ListOfBlogsComponent },
  { path: '*', component: AddBlogsComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
