import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddBlogsComponent } from './components/add-blogs/add-blogs.component';
import { EditBlogComponent } from './components/edit-blog/edit-blog.component';
import { ListOfBlogsComponent } from './components/list-of-blogs/list-of-blogs.component';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { DataService } from './server/data.service';
import { BlogService } from './service/blog.service';
import { CommonSharedService } from './service/common-shared.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    AddBlogsComponent,
    EditBlogComponent,
    ListOfBlogsComponent,
  ],
  imports: [
    BrowserModule, // render in browser
    FormsModule,
    AppRoutingModule, // for routing
    HttpClientModule, //  for http service
    InMemoryWebApiModule.forRoot(DataService),
    NgbModule, // creating database server
  ],
  providers: [BlogService, CommonSharedService], // service injecting
  bootstrap: [AppComponent],
})
export class AppModule {}
